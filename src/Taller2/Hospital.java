package Taller2;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Hospital {

    public String nombreH, tipo;
    int categoria, presupuesto;
    Scanner n = new Scanner(System.in);

    public Hospital() {
        JOptionPane.showMessageDialog(null, "Ingrese el nombre del hospital: ");
        System.out.println("Ingrese el nombre del hospital: ");
        this.nombreH = n.nextLine();
        System.out.println("Ingrese la categoria del hospital (1,2,3,4): ");
        this.categoria = n.nextInt();
        n.nextLine();
        System.out.println("Ingrese el tipo de hospital: ");
        this.tipo = n.nextLine();
        System.out.println("Ingrese el presupuesto del hospital: ");
        this.presupuesto = n.nextInt();
        n.nextLine();
    }
    
    public String getNombreH() {
        return nombreH;
    }

    public void setNombreH(String nombre) {
        this.nombreH = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(int presup) {
        this.presupuesto = presup;
    }

}
