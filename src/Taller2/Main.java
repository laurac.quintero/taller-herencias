package Taller2;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        Scanner n = new Scanner(System.in);
        Main principal = new Main();

        System.out.println("Seleccione el tipo de datos a ingresar: ");
        System.out.println("1- Paciente");
        System.out.println("2- Médico");
        int opt;
        opt = n.nextInt();
        n.nextLine();

        if (opt == 1) {
            principal.informacionPaciente();

        }

        if (opt == 2) {

            principal.informacionMedico();
        }

    }

    public void informacionPaciente() {

        Enfermedad enf = new Enfermedad();

        System.out.print("Nombre paciente: " + enf.getNombre1() + "\n");
        System.out.print("Sexo del paciente: " + enf.getSexo() + "\n");
        System.out.print("Hospital donde lo atendieron: " + enf.getNombreH() + "\n");
        System.out.print("Categoría del hospital: " + enf.getCategoria() + "\n");
        System.out.print("Nombre de la enfermedad: " + enf.getNombre2() + "\n");
        System.out.print("Tratamiento de la enfermedad: " + enf.getMedic() + "\n");

    }

    public void informacionMedico() {

        Medico med = new Medico();

        System.out.print("Hospital donde trabaja: " + med.getNombreH() + "\n");
        System.out.print("Categoría del hospital: " + med.getCategoria() + "\n");
        System.out.print("Tipo de hospital: " + med.getTipo() + "\n");
        System.out.print("Presupuesto del hospital: " + med.getPresupuesto() + "\n");
        System.out.print("ID del médico: " + med.getId() + "\n");
        System.out.print("Sexo: " + med.getSexo1() + "\n");
        System.out.print("Especialidad: " + med.getEsp() + "\n");

    }

}
