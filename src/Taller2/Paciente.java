package Taller2;

public class Paciente extends Hospital {

    String nombre1, sexo;
    int codigo;

    public Paciente() {
        super();
        System.out.println("Ingrese el nombre del paciente: ");
        this.nombre1 = n.nextLine();
        System.out.println("Digite código del paciente: ");
        this.codigo = n.nextInt();
        n.nextLine();
        System.out.println("Ingrese sexo del paciente: ");
        this.sexo = n.nextLine();
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

}
